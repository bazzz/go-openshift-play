# syntax=docker/dockerfile:1
FROM golang:1.22

WORKDIR /app

COPY . .

RUN go build -ldflags "-X main.version=$(git log -1 --format=%cd --date=format:'%Y%m%d')" -o /app/openshiftplay cmd/main.go

EXPOSE 8080

CMD ["/app/openshiftplay"]