package main

import (
	"log"
	"net/http"
)

var version string

func main() {

	log.Println("Version:", version)
	http.HandleFunc("/", getRoot)

	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}

func getRoot(w http.ResponseWriter, r *http.Request) {
	log.Fatal("Version:", version)
}
